﻿using DataAccessLayer.Contexts;
using DataAccessLayer.Entities;
using DataAccessLayer.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TestDataAccessLayerConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Test Data Access Layer console application!");

            using (var uow = new UnitOfWork(new MainContext()))
            {
                var teacher = uow.Teachers.GetById(Guid.Parse("26233870-6fca-42f2-8c19-79788598bf5e"));

                Console.WriteLine(teacher.Name);

                teacher.Name = "Кузьма";

                uow.Teachers.Update(teacher);

                uow.Commit();

                Console.WriteLine(uow.Teachers.GetById(Guid.Parse("26233870-6fca-42f2-8c19-79788598bf5e")).Name);
            }

            Console.ReadKey();
        }
    }
}
