﻿using DataAccessLayer.Enums;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    /// <summary>
    /// Сущность ученика
    /// </summary>
    [Table("Pupil")]
    public class PupilEntity : BaseEntity, IHuman
    {
        /// <summary>
        /// Создать сущность ученика
        /// </summary>
        public PupilEntity() : base()
        {

        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public byte Age { get; set; }
        public EPosition Position { get; set; }

        /// <summary>
        /// Класс
        /// </summary>
        public ClassroomEntity Classroom { get; set; }
    }
}
