﻿using System;

namespace DataAccessLayer.Entities
{
    /// <summary>
    /// Базовый класс для сущностей
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Создать базовую сущность
        /// </summary>
        public BaseEntity()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public Guid Id { get; set; }
    }
}
