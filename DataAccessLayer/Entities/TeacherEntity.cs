﻿using DataAccessLayer.Enums;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    /// <summary>
    /// Сущность преподователя
    /// </summary>
    [Table("Teacher")]
    public class TeacherEntity : BaseEntity, IHuman
    {
        /// <summary>
        /// Создать сущность преподоваетеля
        /// </summary>
        public TeacherEntity() : base()
        {
            Classrooms = new List<TeacherClassroomEntity>();
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public byte Age { get; set; }
        public EPosition Position { get; set; }

        /// <summary>
        /// Классы
        /// </summary>
        public virtual IList<TeacherClassroomEntity> Classrooms { get; set; }
    }
}
