﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    /// <summary>
    /// Сущность класса
    /// </summary>
    [Table("Classroom")]
    public class ClassroomEntity : BaseEntity
    {
        /// <summary>
        /// Создать сущность класса
        /// </summary>
        public ClassroomEntity() : base()
        {
            Pupils = new List<PupilEntity>();
            Teachers = new List<TeacherClassroomEntity>();
        }

        /// <summary>
        /// Номер
        /// </summary>
        public byte Number { get; set; }

        /// <summary>
        /// Буква
        /// </summary>
        public char Letter { get; set; }

        /// <summary>
        /// Ученики
        /// </summary>
        public virtual IList<PupilEntity> Pupils { get; set; }

        /// <summary>
        /// Преподователи
        /// </summary>
        public virtual IList<TeacherClassroomEntity> Teachers { get; set; }
    }
}
