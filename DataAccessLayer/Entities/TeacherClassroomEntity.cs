﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Entities
{
    /// <summary>
    /// Сущность связь многие ко многим - преподователь к классу
    /// </summary>
    public class TeacherClassroomEntity
    {
        /// <summary>
        /// Уникальный идентификатор преподователя
        /// </summary>
        public Guid TeacherId { get; set; }

        /// <summary>
        /// Уникальный идентификатор класса
        /// </summary>
        public Guid ClassroomId { get; set; }

        /// <summary>
        /// Ссылка на преподователя
        /// </summary>
        public TeacherEntity Teacher { get; set; }

        /// <summary>
        /// Ссылка на класс
        /// </summary>
        public ClassroomEntity Classroom { get; set; }
    }
}
