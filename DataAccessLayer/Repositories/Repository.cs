﻿using DataAccessLayer.Contexts;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace DataAccessLayer.Repositories
{
    /// <summary>
    /// Репозиторий - прослойка между логикой и ORM. Дает возможность работы с CRUD операциями.
    /// </summary>
    /// <typeparam name="TEntity">тип сущности наследуемой от BaseEntity</typeparam>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private MainContext _context;
        private DbSet<TEntity> _dbSet;

        /// <summary>
        /// Создать репозиторий
        /// </summary>
        /// <param name="context">контекст</param>
        public Repository(MainContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }

        public IQueryable<TEntity> Entities => _dbSet;

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public TEntity GetById(Guid id)
        {
            return _dbSet.Find(id);
        }

        public void Update(TEntity entity)
        {
            _dbSet.Update(entity);
        }
    }
}
