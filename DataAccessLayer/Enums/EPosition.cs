﻿namespace DataAccessLayer.Enums
{
    /// <summary>
    /// Позиция человека
    /// </summary>
    public enum EPosition
    {
        /// <summary>
        /// Нету
        /// </summary>
        None,
        /// <summary>
        /// Директор
        /// </summary>
        Director,
        /// <summary>
        /// Завуч
        /// </summary>
        HeadTeacher,
        /// <summary>
        /// Староста
        /// </summary>
        HeadPupil
    }
}
