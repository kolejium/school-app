﻿using DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Contexts
{
    /// <summary>
    /// Основной контекст для работы с базой данных
    /// </summary>
    public class MainContext : DbContext
    {
        /// <summary>
        /// Создать контекст
        /// </summary>
        public MainContext() : base()
        {

        }

        #region Tables

        /// <summary>
        /// Классы
        /// </summary>
        public DbSet<ClassroomEntity> Classrooms { get; set; }

        /// <summary>
        /// Ученики
        /// </summary>
        public DbSet<PupilEntity> Pupils { get; set; }

        /// <summary>
        /// Преподователи
        /// </summary>
        public DbSet<TeacherEntity> Teachers { get; set; }

        #endregion

        #region AdditionTables

        /// <summary>
        /// Связь класс - преподователь (многие ко многим)
        /// </summary>
        public DbSet<TeacherClassroomEntity> TeacherClassrooms { get; set; }

        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost; Port=5432; Database=School; Username=postgres; Password=admin");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TeacherClassroomEntity>().HasKey(sc => new { sc.TeacherId, sc.ClassroomId });
        }

    }
}
