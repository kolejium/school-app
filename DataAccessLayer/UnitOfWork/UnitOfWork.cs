﻿using DataAccessLayer.Contexts;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace DataAccessLayer.UnitOfWork
{
    /// <summary>
    /// Единица работы
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MainContext _context;

        #region Переменные

        private IRepository<ClassroomEntity> _classrooms;
        private IRepository<PupilEntity> _pupils;
        private IRepository<TeacherEntity> _teachers;

        #endregion 

        /// <summary>
        /// Создать единицу работы
        /// </summary>
        /// <param name="context">контекст</param>
        public UnitOfWork(MainContext context)
        {
            _context = context;
        }

        #region Свойства

        public IRepository<ClassroomEntity> Classrooms => _classrooms ?? (_classrooms = new Repository<ClassroomEntity>(_context));
        public IRepository<PupilEntity> Pupils => _pupils ?? (_pupils = new Repository<PupilEntity>(_context));
        public IRepository<TeacherEntity> Teachers => _teachers ?? (_teachers = new Repository<TeacherEntity>(_context));

        #endregion

        public void Commit()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void Reject()
        {
            foreach (var item in _context.ChangeTracker.Entries().ToList().Where(x => x.State != EntityState.Unchanged))
            {
                switch (item.State)
                {
                    case EntityState.Added:
                        item.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        item.Reload();
                        break;
                }
            }
        }

        // TODO: can execute many operations by repostiories in one transaction => queue
    }
}
