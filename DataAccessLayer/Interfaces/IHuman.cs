﻿using DataAccessLayer.Enums;

namespace DataAccessLayer.Interfaces
{
    /// <summary>
    /// Описывает основные свойства человека
    /// </summary>
    public interface IHuman
    {
        /// <summary>
        /// Имя
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        string Surname { get; set; }

        /// <summary>
        /// Возраст
        /// </summary>
        byte Age { get; set; }

        /// <summary>
        /// Позиция
        /// </summary>
        EPosition Position { get; set; }
    }
}
