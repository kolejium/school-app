﻿using DataAccessLayer.Entities;
using System;

namespace DataAccessLayer.Interfaces
{
    /// <summary>
    /// Описывает основные методы единицы работы (паттер UnitOfWork)
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        #region Репозитории

        IRepository<PupilEntity> Pupils { get; }
        IRepository<ClassroomEntity> Classrooms { get; }
        IRepository<TeacherEntity> Teachers { get; }

        #endregion

        /// <summary>
        /// Принять изменения
        /// </summary>
        void Commit();

        /// <summary>
        /// Отменить изменения
        /// </summary>
        void Reject();
    }
}
