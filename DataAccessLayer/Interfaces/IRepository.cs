﻿using DataAccessLayer.Entities;
using System;
using System.Linq;

namespace DataAccessLayer.Interfaces
{
    /// <summary>
    /// Описывает основные методы и свойства репозитория
    /// </summary>
    /// <typeparam name="TEntity">тип сущности наследуемый от BaseEntity</typeparam>
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        /// Добавить сущность
        /// </summary>
        /// <param name="entity">сущность</param>
        void Add(TEntity entity);

        /// <summary>
        /// Изменить сущность
        /// </summary>
        /// <param name="entity">сущность</param>
        void Update(TEntity entity);

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="entity">сущность</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Получить сущность по уникальному идентификатору
        /// </summary>
        /// <param name="id">уникальный идентификатор</param>
        /// <returns>сущность</returns>
        TEntity GetById(Guid id);

        /// <summary>
        /// Сущности
        /// </summary>
        IQueryable<TEntity> Entities { get; }

        // TODO: Get Entity(ies) by query
    }
}
